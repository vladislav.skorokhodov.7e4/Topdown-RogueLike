using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnObject : MonoBehaviour
{
    public GameObject[] llistaDrops;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Bullet"))
        {
            int randomItems = (int)Random.Range(0, llistaDrops.Length+1);
            Instantiate(llistaDrops[randomItems], this.gameObject.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}
