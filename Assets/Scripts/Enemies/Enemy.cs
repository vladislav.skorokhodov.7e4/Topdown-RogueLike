using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    public float speed;
    public float health;
    public int damage;
    public float impulse;
    public GameObject self;

    Rigidbody2D rb;
    private void Start()
    {
       
    }
    public void follow()
    {
        Transform target = GameObject.FindGameObjectWithTag("Player").transform;
        self.transform.position = Vector3.MoveTowards(new Vector3 (self.transform.position.x, self.transform.position.y, 20), new Vector3(target.transform.position.x, target.transform.position.y, 20) , speed * Time.deltaTime);
    }

    void Update()
    {
        follow();
        if (health <= 0)
        {
            Destroy(gameObject);
            PlayerManager.instance.getPoints(50);
            PlayerManager.instance.getCoins(10);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerManager.instance.changeCurrentHealth(-damage);
            rb = collision.gameObject.GetComponent<Rigidbody2D>();
            rb.AddForce((collision.gameObject.transform.position - self.transform.position) * impulse, ForceMode2D.Impulse);
        }
    }

    public void getHurt(float dam)
    {
        health -= dam;
        PlayerManager.instance.getPoints(1);
    }
}
