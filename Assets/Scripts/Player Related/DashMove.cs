using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashMove : MonoBehaviour
{

    public float dashPower = 5f;
    private Rigidbody2D rb;

    public  float cooldown = 2f;
    private float nextUse;

    public float Invulcooldown = 0.125f;
    private float nextInv;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space ))
        {
            if (Input.GetAxisRaw("Horizontal") > 0 && Input.GetAxis("Vertical") == 0 && Time.time > nextUse) //Right
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(dashPower, 0), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") > 0 && Input.GetAxis("Vertical") > 0 && Time.time > nextUse) //Right Up
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(dashPower, dashPower), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") > 0 && Input.GetAxis("Vertical") < 0 && Time.time > nextUse) //Right Down
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(dashPower, -dashPower), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") < 0 && Input.GetAxis("Vertical") == 0 && Time.time > nextUse) //Left
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(-dashPower, 0), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") < 0 && Input.GetAxis("Vertical") > 0 && Time.time > nextUse) //Left Up
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(-dashPower, dashPower), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") < 0 && Input.GetAxis("Vertical") < 0 && Time.time > nextUse) //Left Down
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(-dashPower, -dashPower), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") < 0 && Time.time > nextUse) //Down
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, -dashPower), ForceMode2D.Force);
            }
            else if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") > 0 && Time.time > nextUse) //Up
            {
                nextInv = Time.time + Invulcooldown;
                rb.GetComponent<Collider2D>().isTrigger = true;
                rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

                nextUse = Time.time + cooldown;
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, dashPower), ForceMode2D.Force);
            }
        }

        if (Time.time > nextInv)
        {
            rb.GetComponent<Collider2D>().isTrigger = false;
            rb.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1f);
        }
        
    }
}
