using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Weapon weaponInfo;
    public Transform firePoint;

    public float bulletForce = 20;

    private float insidefirerate;
    private float nextFire = 0f;


    private void Start()
    {
        weaponInfo.bullets = weaponInfo.starterBullets;
    }

    void Update()
    {

        insidefirerate = 1 / weaponInfo.firerate;
        if (Input.GetButton("Fire1") && Time.time > nextFire && weaponInfo.bullets>0)
        {
            Shoot();
        }
        
    }

    void Shoot()
    {
        weaponInfo.bullets--;
        nextFire = Time.time + insidefirerate;
        GameObject bullet =  Instantiate(weaponInfo.bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * weaponInfo.bulletForce, ForceMode2D.Impulse);
        

    }
   
}
