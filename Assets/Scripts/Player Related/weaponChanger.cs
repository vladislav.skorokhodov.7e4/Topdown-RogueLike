
using System;
using UnityEngine;

public class weaponChanger : MonoBehaviour
{

    public int currentWeaponHere;

    private void Start()
    {
        SelectWeapon();
    }

   

    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == currentWeaponHere)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            i++;
        }
    }

    void Update()
    {
        PlayerManager.instance.currentWeapon = currentWeaponHere;

        int previousSelectedWeapon = currentWeaponHere;

        if (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetKeyDown(KeyCode.X))
        {
            if (currentWeaponHere >= transform.childCount - 1)
                currentWeaponHere = 0;
            else
                currentWeaponHere++;

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0 || Input.GetKeyDown(KeyCode.Z))
        {
            if (currentWeaponHere <= 0)
                currentWeaponHere = transform.childCount-1;
            else
                currentWeaponHere--;
        }

        if (previousSelectedWeapon != currentWeaponHere)
        {
            SelectWeapon();
        }
    }
}
