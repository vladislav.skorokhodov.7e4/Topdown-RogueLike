using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerStats : MonoBehaviour
{
    public int maxHealh = 100;
    public int currentHealth;

    public HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealh;
        healthBar.SetMaxHealh(maxHealh);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void changeCurrentHealth(int changer)
    {
        currentHealth += changer;

        if ((currentHealth + changer) > maxHealh)
        {
            currentHealth = maxHealh;
        }
        healthBar.SetHealth(currentHealth);

    }
}
