using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float moveSpeed = 5f;
    public Rigidbody2D rb;
    Vector2 movement;
    public Camera came;
    Vector2 mousePos;



    public float dashPower = 5f;
    //private float dashTime;
    //public float startDashTime;
    //private int direction;

    private void Start()
    {
        //dashTime = startDashTime;
    }

    void Update()
    {
        
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        

        mousePos = came.ScreenToWorldPoint(Input.mousePosition);

        /*
        if (direction == 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = 1;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = 2;
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = 3;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                direction = 4;
            }
        } else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            } else
            {
                dashTime -= Time.deltaTime;

                if(direction == 1)
                {
                    rb.velocity = Vector2.left * dashPower;
                } else if(direction == 2)
                {
                    rb.velocity = Vector2.right * dashPower;
                } else if(direction == 3)
                {
                    rb.velocity = Vector2.up * dashPower;
                } else if (direction == 4)
                {
                    rb.velocity = Vector2.down * dashPower;
                }
            }
        }
        */


        
    }

    private void FixedUpdate()
    {
        rb.transform.position = (rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        //rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90;
        rb.rotation = angle;
    }
}
