using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public float timeToLive;
    public int damage;

    //public GameObject hitEffect;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        //Destroy(effect, 5f);
        Destroy(gameObject);
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<Enemy>().getHurt(damage + damage * PlayerManager.instance.dmgMult);
        }
    }

    private void Update()
    {
        Destroy(gameObject, timeToLive);
    }
}
