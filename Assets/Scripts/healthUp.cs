using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthUp : MonoBehaviour
{
    public bool buyable;
    public int price = 15;

    public int healtModificator = 20;
    public Text text;


    private void Update()
    {
        text.text = ""+ price;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerStats playerStats = new playerStats();
        if (collision.gameObject.CompareTag("Player") )
        {
            if (buyable == false)
            {
                PlayerManager.instance.changeCurrentHealth(healtModificator);
                PlayerManager.instance.getPoints(20);
                Destroy(gameObject);
            }
            else if (buyable == true && PlayerManager.instance.Coins >= price)
            {
                PlayerManager.instance.getCoins(-price);
                PlayerManager.instance.changeCurrentHealth(healtModificator);
                PlayerManager.instance.getPoints(20);
                Destroy(gameObject);
            }
            else
            {

            }
            
        }
    }
}
