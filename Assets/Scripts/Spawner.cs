using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] llistaEnemies;
    [SerializeField]
    public float summonRadius = 1;
    public float spawnerCooldown = 1;
    public float startSpawnInSec = 5;
    public int enemyToSpawn;
    public int randomEnemy;


    void Start()
    {
        
        int chance = (int)Random.Range(0, 10);
        if(chance <= 4 && chance > 0)
        {
            randomEnemy = 0;
        }
        else if(chance > 4)
        {
            randomEnemy = 1;
        }
        else if(chance >= 0 && chance < 2)
        {
            randomEnemy = 2;
        }
        
        CreateEnemy();
        Instantiate(llistaEnemies[randomEnemy], new Vector3((gameObject.transform.position.x + Random.Range(-summonRadius, summonRadius)), (gameObject.transform.position.y + Random.Range(-summonRadius, summonRadius)), 0), Quaternion.identity);

        /*if (randomEnemy == 0)
        {
            InvokeRepeating("CreateEnemy", 2, spawnerCooldown);
        }*/
        
        //self.position = gameObject.transform.position;
    }


    private void CreateEnemy()
    {
        //Instantiate(enemy, new Vector3(Random.Range(self.position.x - summonRadius, self.position.x + summonRadius), Random.Range(self.position.y - summonRadius, self.position.y + summonRadius), 0), Quaternion.identity);
        Instantiate(llistaEnemies[1], new Vector3((gameObject.transform.position.x + Random.Range(-summonRadius, summonRadius)), (gameObject.transform.position.y + Random.Range(-summonRadius, summonRadius)), 0), Quaternion.identity);

    }
}
