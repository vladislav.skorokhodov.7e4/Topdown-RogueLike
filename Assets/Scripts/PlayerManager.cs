using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;
    public GameObject texr;

    //Bullets
    public int startingPistolBullets = 50;
    public int startingrifleBullets = 50;
    public int startingshotgunBullets =30;
    public int startingAMPBullets = 5;
    public int startingsniperBullets = 10;
    
    //HP
    public int maxHealh = 100;
    public int currentHealth;

    //Misc
    public int currentWeapon;
    public int Points;
    public int Coins;
    public float dmgMult = 1f;

    public HealthBar healthBar;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    

    void Start()
    {
        

        currentHealth = maxHealh;
        healthBar.SetHealth(currentHealth);
    }

    void Update()
    {
        if (currentHealth > maxHealh)
        {
            currentHealth = maxHealh;
        }

        healthBar.SetMaxHealh(maxHealh);
        
        Debug.Log("weapon" + currentWeapon); // A eliminar
        if (currentHealth <= 0)
        {
            
            Destroy(gameObject);
            texr.SetActive(true);
            

        }
    }

    public void changeCurrentHealth(int changer)
    {
        currentHealth += changer;

        if ((currentHealth + changer) > maxHealh)
        {
            currentHealth = maxHealh;
        }
        healthBar.SetHealth(currentHealth);

    }

    public void getPoints(int quantity)
    {
        Points += quantity;
    }

    public void getCoins (int quantity)
    {
        Coins += quantity;
    }
}
