using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBulletScript : MonoBehaviour
{
    public float timeToLive;
    public int damage = 0;

    //public GameObject hitEffect;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerManager>().changeCurrentHealth(-damage);
        }
    }

    private void Update()
    {
        Destroy(gameObject, timeToLive);
    }
}
