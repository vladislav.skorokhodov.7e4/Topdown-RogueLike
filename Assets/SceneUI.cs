using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneUI : MonoBehaviour
{
    ManagerOfSins scene = new ManagerOfSins();

    public void LoadMenu()
    {
        scene.CurrentScene = ManagerOfSins.Scenes.Menu;
    }

    public void LoadGame()
    {
        //Debug.Log("GamePressingBUTTOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOON");
        scene.CurrentScene = ManagerOfSins.Scenes.Game;
    }

    public void LoadScore()
    {
        scene.CurrentScene = ManagerOfSins.Scenes.Score;
    }
}
