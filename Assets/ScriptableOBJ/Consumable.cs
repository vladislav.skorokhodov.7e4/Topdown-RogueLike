using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Consumable : ITEMS
{
    public abstract void OnTakeEffect();
}
