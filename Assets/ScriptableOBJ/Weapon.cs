using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Items/Weapon", order = 1)]
public class Weapon : ITEMS
{
    public GameObject bulletPrefab;
    public float bulletForce = 20;
    public float firerate = 1;
    public int maxBullets;
    public int bullets;
    public int starterBullets;
}
