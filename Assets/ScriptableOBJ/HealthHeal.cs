using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heal", menuName = "ScriptableObjects/Items/Consumable/Heal", order = 1)]
public class HealthHeal : Consumable
{
    public int pointsToHeal;
    public override void OnTakeEffect()
    {
        PlayerManager.instance.changeCurrentHealth(pointsToHeal);
    }
}
