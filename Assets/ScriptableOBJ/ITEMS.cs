using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITEMS : ScriptableObject
{
    public int preu;
    public itemType typeOfItem;
    public string nom;
    public bool buyable;
    public int pointsToGive;
    public Sprite image;
    public enum itemType
    {
        consummable,
        weapon,
        statUp
    }
}
