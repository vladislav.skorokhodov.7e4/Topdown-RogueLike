using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Stats", menuName = "ScriptableObjects/Items/StatUPS", order = 1)]
public class StatUps : ITEMS

{
    public float statNumChangeMasOMesnos;
    public enum statType
    {
        MaxHP,
        DamageMultiplier
    }

    public statType typeOfStat;

    public void StatChange()
    {
        if(typeOfStat == statType.MaxHP)
        {
            PlayerManager.instance.maxHealh += Mathf.FloorToInt(statNumChangeMasOMesnos);
        } 
        else if(typeOfStat == statType.DamageMultiplier)
        {
            PlayerManager.instance.dmgMult += statNumChangeMasOMesnos;
        }
    }
}
