using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Ammo", menuName = "ScriptableObjects/Items/Consumable/Ammo", order = 1)]
public class Ammo : Consumable
{
    public bulletType typeOfBullet;
    public enum bulletType
    {
        pistol,
        rifle,
        siperRifle,
        shotgun,
        rocket
    }
    public int bulletNumGive;
    public override void OnTakeEffect()
    {
        if (typeOfBullet == bulletType.pistol)
        {

        }
        else if (typeOfBullet == bulletType.rifle)
        {

        }
        else if (typeOfBullet == bulletType.siperRifle)
        {

        }
        else if (typeOfBullet == bulletType.shotgun)
        {

        }
        else if (typeOfBullet == bulletType.rocket)
        {

        }
    }
}
