using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onTouch : MonoBehaviour
{
    public ITEMS items;


    private void Start()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = items.image;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            switch (items.typeOfItem)
            {
                case ITEMS.itemType.consummable:
                    Consumable c = (Consumable)items;
                    c.OnTakeEffect();
                    break;


                case ITEMS.itemType.statUp:
                    StatUps s = (StatUps)items;
                    s.StatChange();
                    break;


                default:
                    Debug.Log("Error");
                    break;
            }
        }
    }
}
