using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private static int _score;
    public static int Score
    {
        get
        {
            return _score;
        }
    }

    private void Start()
    {
        _score = 0;
    }

    private void Awake() { 

        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }



    public int getScore()
    {
        Debug.Log("Score: " + Score);
        return Score;
    }


    public void ModifyScore(int value)
    {
        _score = value;
        Debug.Log("Score: " + Score);
    }
}
