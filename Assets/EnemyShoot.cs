using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public GameObject self;

    float angle;
    public GameObject bullet;
    public Transform puntDeDispar;
    public Rigidbody2D rb;
    //public Transform attacker;
    // public Transform offset;

    public float bulletSpeed = 10;

    public float timeToShoot = 1;
    private float nextFire = 0f;



    // Update is called once per frame
    void Update()
    {
        Transform target = GameObject.FindGameObjectsWithTag("Player")[0].transform;

        Vector2 lookdir = puntDeDispar.position - target.position;
        angle = Mathf.Atan2(lookdir.y, lookdir.x) * Mathf.Rad2Deg;

        rb.rotation = angle - 90;

        if (Time.time > nextFire)
        {

            GameObject bulletClone = Instantiate(bullet);


            bulletClone.transform.position = puntDeDispar.position;
            bulletClone.transform.rotation = Quaternion.Euler(0, 0, angle + 90);

            bulletClone.GetComponent<Rigidbody2D>().velocity = puntDeDispar.right * bulletSpeed;




            nextFire = Time.time + timeToShoot;

        }
    }
}