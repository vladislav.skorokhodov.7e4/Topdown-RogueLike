using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text text;
    private void Update()
    {
        text.text = "Coins " + PlayerManager.instance.Coins;
    }
}
