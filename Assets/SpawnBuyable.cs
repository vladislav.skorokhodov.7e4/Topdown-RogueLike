using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBuyable : MonoBehaviour
{
    public GameObject[] llistaDrops;
    private void Start()
    {
        int randomItems = (int)Random.Range(0, llistaDrops.Length);
        Instantiate(llistaDrops[randomItems], this.gameObject.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
