using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateContentRoom : MonoBehaviour
{
    public GameObject[] llistaRooms;


    // Start is called before the first frame update
    void Start()
    {
        int randomItems = (int)Random.Range(0, llistaRooms.Length + 1);
        Instantiate(llistaRooms[randomItems], this.gameObject.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
